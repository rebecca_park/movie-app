-- MySQL dump 10.13  Distrib 5.7.17, for osx10.11 (x86_64)
--
-- Host: localhost    Database: movieApp
-- ------------------------------------------------------
-- Server version	5.7.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `movie`
--

DROP TABLE IF EXISTS `movie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movie` (
  `movie_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text,
  `year` int(11) DEFAULT NULL,
  `poster` text,
  `coverPhoto` text,
  `storyline` text,
  PRIMARY KEY (`movie_id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie`
--

LOCK TABLES `movie` WRITE;
/*!40000 ALTER TABLE `movie` DISABLE KEYS */;
INSERT INTO `movie` VALUES (1,'Pulp Fiction',1994,'/img/PULP-FICTION-2100.jpg','/img/pulp_fiction.png','blah'),(2,'Rogue One: Star Wars Story',2016,'/img/rogueOne.jpg','',''),(3,'Star Wars: Episode I',2000,'/img/star_wars.png','',''),(4,'Star Wars: Episode II',2000,'/img/sw_2.jpg','',''),(5,'Star Wars: Episode III',2000,'/img/Starwars_3.jpg','',''),(11,'In Bruges',2008,'/img/in_bruges.jpg','/img/in_bruges_cover.jpg','blah'),(14,'2046',2004,'/img/2046_poster.jpg','/img/2046_cover.jpg','blah'),(15,'Finding Dory',2016,'/img/Finding_Dory_poster.jpg','/img/finding_dory_cover.jpg','blah'),(17,'Finding Nemo',2003,'/img/finding_nemo_poster.jpg','/img/finding_nemo_cover.jpg','blah'),(18,'Oldboy',2003,'/img/oldboy_poster.jpg','/img/oldboy_cover.jpg',''),(28,'The Lego Movie',2014,'/img/legoMovie_poster.jpeg','/img/legoMovie_cover.png','blah'),(30,'Arahan',2001,'','','blah'),(44,'Legally Blond',2010,'','','blah');
/*!40000 ALTER TABLE `movie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movie_people`
--

DROP TABLE IF EXISTS `movie_people`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movie_people` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `movie_id` int(11) DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  `role` text,
  PRIMARY KEY (`row_id`),
  KEY `movie_id` (`movie_id`),
  KEY `person_id` (`person_id`),
  CONSTRAINT `movie_people_ibfk_1` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`movie_id`),
  CONSTRAINT `movie_people_ibfk_2` FOREIGN KEY (`person_id`) REFERENCES `people` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie_people`
--

LOCK TABLES `movie_people` WRITE;
/*!40000 ALTER TABLE `movie_people` DISABLE KEYS */;
INSERT INTO `movie_people` VALUES (1,18,5,'Director'),(2,28,11,'Director'),(11,44,30,'Elle Woods'),(20,1,39,'Director'),(21,1,40,'Mia Wallace'),(22,1,41,'Vincent Vega'),(23,1,42,'Jules Winnfield'),(24,1,43,'Butch Coolidge'),(25,1,39,'Jimmie Dimmick'),(26,15,1,'Director'),(27,17,1,'Director'),(28,17,2,'Dory'),(29,17,4,'Nemo'),(30,17,1,'Crush'),(31,17,3,'Director');
/*!40000 ALTER TABLE `movie_people` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `people`
--

DROP TABLE IF EXISTS `people`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `people` (
  `person_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `picture` text,
  PRIMARY KEY (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `people`
--

LOCK TABLES `people` WRITE;
/*!40000 ALTER TABLE `people` DISABLE KEYS */;
INSERT INTO `people` VALUES (1,'Andrew Stanton','/img/andrew_stanton.jpg'),(2,'Ellen Degeneres','/img/Ellen-Degeneres.jpg'),(3,'Lee Unkrich','/img/LeeUnkrich.jpg'),(4,'Alexander Gould','/img/alexander_gould.jpg'),(5,'Chan-wook Park','/img/chanwookPark.jpg'),(11,'Phil Lord','phil_lord.jpg'),(30,'Reese Withersome',''),(31,'John Williams',''),(39,'Quentin Tarantino','/img/Q_T.png'),(40,'Uma Thurman','/img/uma.jpg'),(41,'John Travolta','/img/JohnTravolta.jpg'),(42,'Samuel L. Jackson','/img/samuel_jackson.jpg'),(43,'Bruce Willis','/img/bruce_willis.jpg');
/*!40000 ALTER TABLE `people` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `username` varchar(200) DEFAULT NULL,
  `password` text,
  `picture` text,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('philneaton95@gmail.com','effieEaton','/img/phil.jpg',1),('burugirl93@gmail.com','mumuEaton','/img/jyp.jpg',2),('rebecca.jy.park@gmail.com','poroporo','',3),('burugirl93@hanmail.net','mumumu','',4),('mumu@gmail.com','coco93co','',5);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_movie`
--

DROP TABLE IF EXISTS `user_movie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_movie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `movie_id` int(11) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  `category` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userID_movieID` (`user_id`,`movie_id`),
  KEY `movie_id` (`movie_id`),
  CONSTRAINT `user_movie_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `user_movie_ibfk_2` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`movie_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_movie`
--

LOCK TABLES `user_movie` WRITE;
/*!40000 ALTER TABLE `user_movie` DISABLE KEYS */;
INSERT INTO `user_movie` VALUES (1,1,1,3,'Watched'),(2,1,2,4,'Watched'),(16,1,4,5,'Watched'),(18,1,17,0,'Want to watch'),(26,1,15,2,'Watched');
/*!40000 ALTER TABLE `user_movie` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-25 19:08:42
