function starMouseOver(star, nthStar) {
    return function() {
	const parent = star.parentNode;
	const stars = parent.querySelectorAll('.fa');
	for (let i=0; i < 5; i++) {
	    if (i <= nthStar) {
		stars[i].classList.add('fa-star');
		stars[i].classList.remove('fa-star-o');
	    } else {
		stars[i].classList.add('fa-star-o');
		stars[i].classList.remove('fa-star');
	    }
	}
    }
}

function starMouseOut(star, nthStar) {
    return function() {
	console.log('here0-1');
	const parent = star.parentNode;
	const stars = parent.querySelectorAll('.fa');
	if (parent.classList.contains('clicked')) {
	    console.log('here0');
	    let clickedStarIndexNumber = 0;
	    for (let i=0; i < 5; i++) {
		if(stars[i].classList.contains('clicked')) {
		    clickedStarIndexNumber = i;
		}
	    }
	    for (let i=0; i < 5; i++) {
		if(i <= clickedStarIndexNumber) {
		    stars[i].classList.add('fa-star');
		    stars[i].classList.remove('fa-star-o');
		} else {
		    stars[i].classList.remove('fa-star');
		    stars[i].classList.add('fa-star-o');
		}
	    }
	    console.log('here1');
	} else {
	    for(let i=0; i < 5; i++) {
		stars[i].classList.remove('fa-star');
		stars[i].classList.add('fa-star-o');
	    }
	    console.log('here2');
	}
    }
}

function starClicked(clickedStar, nthAmong5Stars) {
    return function() {
	const parent = clickedStar.parentNode;
	const allStars = parent.querySelectorAll('.fa');
	let caseNumber = 0;
	//#caseNumber == 1
	//#caseNumber == 2
	
	if (parent.classList.contains('clicked')){
	    //#1: Remove rating from a rated movie
	    if (clickedStar.classList.contains('clicked')) {
		clickedStar.classList.remove('clicked');
		parent.classList.remove('clicked');
		for (let i=0; i < 5; i++) { //Make all the stars to empty stars
		    allStars[i].classList.add('fa-star-o');
		    allStars[i].classList.remove('fa-star');
		}
		caseNumber = 1;
	    } else {//#2: Rerate a rated movie
		clickedStar.classList.add('clicked');
		for (let i=0; i < 5; i++) {
		    if(i < nthAmong5Stars) {
			allStars[i].classList.add('fa-star');
			allStars[i].classList.remove('fa-star-o');
			allStars[i].classList.remove('clicked');
		    } else if (i == nthAmong5Stars) {
			allStars[i].classList.add('fa-star');
		    } else {
			allStars[i].classList.remove('fa-star');
			allStars[i].classList.add('fa-star-o');
			allStars[i].classList.remove('clicked');
		    }
		}
		caseNumber=2;
	    }
	    //#3: Rate the movie that has not been rated
	} else {
	    parent.classList.add('clicked');
	    clickedStar.classList.add('clicked');
	    for (let i=0; i < 5; i++) {
		if (i <= nthAmong5Stars) {
		    allStars[i].classList.add('fa-star');
		} else {
		    allStars[i].classList.remove('fa-star');
		    allStars[i].classList.add('fa-star-o');
		}
	    }
	    caseNumber=2;
	}
      if (caseNumber==1) {
        request("POST", "/movie/" + parent.dataset.movieId + "/unrate", function() {
          var title=parent.dataset.movieTitle;
          title=title.split(" ");
          title=title.join("-");
          window.location="/movie/" + title;
        });
        } else if (caseNumber == 2) {
	    request("POST", "/movie/" + parent.dataset.movieId + "/rate", {
		rating: nthAmong5Stars + 1,
	    });
	    console.log(nthAmong5Stars + 1, 'this is rating');
	} else {
	    request("POST", "/movie/" + parent.dataset.movieId + "/rate", {
		rating: 0,
	    });
	}
	return false;
    }
}

const star = document.querySelectorAll('.fa-star, .fa-star-o') ;
for (let i=0; i < star.length; i++) {
    star[i].onmouseover = starMouseOver(star[i], i);
    star[i].onmouseout = starMouseOut(star[i], i);
    star[i].onclick = starClicked(star[i], i);
}
