function addDirectorClick(button) {
  return function() {
    const formRow = button.closest('.Form-row');
    const secondToLastRow = formRow.querySelector(".row:nth-last-of-type(2)");
    
    const clone = secondToLastRow.cloneNode(true);
    const inputs = clone.querySelectorAll('input');

    for(let i=0; i < inputs.length; i++) {
      inputs[i].value="";
      const [name, number] = inputs[i].name.split("-");
      //inputs[i].name = name + '-' + (+number + 1); 
    }
    formRow.insertBefore(clone, secondToLastRow.nextSibling);
  }
}

function addCastClick(button) {
  return function() {
    const formRow = button.closest(".Form-row");
    const secondToLastRow = formRow.querySelector("div:first-of-type");
        
    const cloneSecondToLast = secondToLastRow.cloneNode(true);
    const inputsSecondToLast = cloneSecondToLast.querySelectorAll('input');
    
    for(let i=0; i < inputsSecondToLast.length; i++) {
      inputsSecondToLast[i].value="";
    }
    formRow.insertBefore(cloneSecondToLast, secondToLastRow.nextSibling);
  }
}

const addCast = document.querySelector('.AddMovie-addCast');
addCast.onclick = addCastClick(addCast);

const addDirector = document.querySelector('.AddMovie-addDirector');
addDirector.onclick = addDirectorClick(addDirector);
