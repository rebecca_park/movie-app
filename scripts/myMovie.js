function movieMouseOver(movie) {
  return function() {
    const hidden = movie.querySelector('.Movie-stars');
    hidden.classList.remove('Movie-stars--hidden');
  }
}

function movieMouseOut(movie) {
  return function() {
    const hidden = movie.querySelector('.Movie-stars');
    const stars = hidden.querySelectorAll('.fa');
    if (!hidden.classList.contains('clicked')) {
      for (let i=0; i < 5; i++) {
        stars[i].classList.add('fa-star-o');
        stars[i].classList.remove('fa-star');
        hidden.classList.add('Movie-stars--hidden');
      }
    }
  }
}

function starMouseOver(star, nthStar) {
  return function() {
    const parent = star.parentNode;
    const stars = parent.querySelectorAll('.fa');
    for (let i=0; i < 5; i++) {
      if (i <= nthStar) { //replace empty stars with filled stars up until the hovered star
        stars[i].classList.add('fa-star');
        stars[i].classList.remove('fa-star-o');
      } 
    }
  }
}

function starMouseOut(star, nthStar) {
  return function() {
   const parent = star.parentNode;
   const stars = parent.querySelectorAll('.fa');
    if (parent.classList.contains('clicked')) { //The movie has been rated
      let clickedStarIndexNumber = 0;
      for (let i=0; i < 5; i++) {
        if(stars[i].classList.contains('clicked')) {
          clickedStarIndexNumber = i;
        }
      }
      for (let i=0; i < 5; i++) {
        if(i<=clickedStarIndexNumber) {
          stars[i].classList.add('fa-star');
        } else {
          stars[i].classList.remove('fa-star');
          stars[i].classList.add('fa-star-o');
        }
      }
    } else { //The movie has not been rated
     star.classList.remove('fa-star');
     star.classList.add('fa-star-o');
   }
  }
}

function starClicked(star, nthStar) {
  return function() {
    const parent = star.parentNode;
    const stars = parent.querySelectorAll('.fa');
    if (star.classList.contains('clicked')) {
      star.classList.remove('clicked');
      parent.classList.remove('clicked');
      for (let i=0; i < 5; i++) {
        stars[i].classList.add('fa-star-o');w
        stars[i].classList.remove('fa-star')
      }
    } else {
      parent.classList.add('clicked');
      star.classList.add('clicked');
      for (let i=0; i < 5; i++) {
        if (i <= nthStar) {
        stars[i].classList.add('fa-star');
        } else {
          stars[i].classList.remove('fa-star');
          stars[i].classList.add('fa-star-o');
        }
      }
    }

    request("POST", "/profile/" + parent.dataset.movieId + "/watched", {
      rating: nthStar + 1,
    });


  }
}

const movie = document.querySelectorAll('.Movie');

for (let i = 0; i < movie.length; i++) {
  const star = movie[i].querySelectorAll('.fa');
  for (let n = 0; n < 5; n++) {
    star[n].onmouseover = starMouseOver(star[n], n);
    star[n].onmouseout = starMouseOut(star[n], n);
    star[n].onclick = starClicked(star[n], n);
  }
}

