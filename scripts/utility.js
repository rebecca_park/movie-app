function request(method, url, data, success, fail) {
  const http = new XMLHttpRequest();
  http.open(method, url, true);

  http.setRequestHeader('Content-Type', 'application/json');

  http.onreadystatechange = function() {
    if (http.readyState == 4 && http.status == 200) {
      success && success(http);
    } else {
      fail && fail(http);
    }
  };

  http.send(JSON.stringify(data));
}
