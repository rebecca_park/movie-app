function movieMouseOver(movie) {
  return function() {
    const hidden = movie.querySelector('.Movie-stars');
    hidden.classList.remove('Movie-stars--hidden');
  }
}

function movieMouseOut(movie) {
  return function() {
    const hidden = movie.querySelector('.Movie-stars');
    const stars = hidden.querySelectorAll('.fa');
    if (!hidden.classList.contains('clicked')) {
      for (let i=0; i < 5; i++) {
        stars[i].classList.add('fa-star-o');
        stars[i].classList.remove('fa-star');
        hidden.classList.add('Movie-stars--hidden');
      }
    }
  }
}

function starMouseOver(star, nthStar) {
  return function() {
    const parent = star.parentNode;
    const stars = parent.querySelectorAll('.fa');
    for (let i=0; i < 5; i++) {
      if (i <= nthStar) { //replace empty stars with filled stars up until the hovered star
        stars[i].classList.add('fa-star');
        stars[i].classList.remove('fa-star-o');
      }
    }
  }
}

function starMouseOut(star, nthStar) {
  return function() {
   const parent = star.parentNode;
   const stars = parent.querySelectorAll('.fa');
    if (parent.classList.contains('clicked')) { //The movie has been rated
      let clickedStarIndexNumber = 0;
      for (let i=0; i < 5; i++) {
        if(stars[i].classList.contains('clicked')) {
          clickedStarIndexNumber = i;
        }
      }
      for (let i=0; i < 5; i++) {
        if(i<=clickedStarIndexNumber) {
          stars[i].classList.add('fa-star');
        } else {
          stars[i].classList.remove('fa-star');
          stars[i].classList.add('fa-star-o');
        }
      }
    } else { //The movie has not been rated
      console.log('This is the case')
      star.classList.remove('fa-star');
      star.classList.add('fa-star-o');
   }
  }
}

function starClicked(clickedStar, nthAmong5Stars) {
  return function(e) {
    // class="Movie-stars"
    const parent = clickedStar.parentNode;
    //selects all the stars within one movie
    const allStars = parent.querySelectorAll('.fa'); 
    let caseNumber = 0;
    //Removes the class 'clicked' from the clickedStar if that class name exists on the clickedStar.
    //Having the class 'clicked' would mean that the stars have been colored up to that star.
    //#2: The
    if (parent.classList.contains('clicked')){
      //#1: Remove rating from a rated movie
      if (clickedStar.classList.contains('clicked')) { 
        clickedStar.classList.remove('clicked');
        parent.classList.remove('clicked');
        for (let i=0; i < 5; i++) { //Make all the stars to empty stars
          allStars[i].classList.add('fa-star-o');
          allStars[i].classList.remove('fa-star')
        }
       caseNumber = 1; 
      } else { //#2: Rerate a rated movie 
        clickedStar.classList.add('clicked');
        console.log('I am in case 2')
        for (let i=0; i<5; i++) {
          if(i < nthAmong5Stars) {
            allStars[i].classList.add('fa-star');
            allStars[i].classList.remove('fa-star-o');
            allStars[i].classList.remove('clicked');
          } else if (i == nthAmong5Stars) {
            allStars[i].classList.add('fa-star');
          } else {
            allStars[i].classList.remove('fa-star');
            allStars[i].classList.add('fa-star-o');
            allStars[i].classList.remove('clicked');
          }
        }
        caseNumber=2;
      } 
    //#3: Rate the movie that has not been rated
    } else { 
      parent.classList.add('clicked');
      clickedStar.classList.add('clicked');
      for (let i=0; i < 5; i++) {
        if (i <= nthAmong5Stars) {
        allStars[i].classList.add('fa-star');
        } else {
          allStars[i].classList.remove('fa-star');
          allStars[i].classList.add('fa-star-o');
        }
      }
      caseNumber=2;
    }
    if (caseNumber == 1) {
      request("POST", "/movie/" + parent.dataset.movieId + "/unrate",  
      {});
    } else if (caseNumber==2) {
      request("POST", "/movie/" + parent.dataset.movieId + "/rate", {
        rating: nthAmong5Stars + 1,
      });
    //} else {
      //request("POST", "/movie" + parent.dataset.movieId + "/rate", {
        //rating: 0,
      //});
    }
    return false;
  }
}


const movie = document.querySelectorAll('.Movie');
for (let i = 0; i < movie.length; i++) {
  movie[i].onmouseover = movieMouseOver(movie[i]);
  movie[i].onmouseout = movieMouseOut(movie[i]);
  const star = movie[i].querySelectorAll('.fa');
  for (let n = 0; n < 5; n++) {
    star[n].onmouseover = starMouseOver(star[n], n);
    star[n].onmouseout = starMouseOut(star[n], n);
    star[n].onclick = starClicked(star[n], n);
  }
}

