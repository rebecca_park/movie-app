let email = document.getElementById('email');
//const email = document.signUpForm.email.value;
let password = document.getElementById('password');
//let password = document.signUpForm.password.value;
let confirmPassword = document.getElementById('confirmPassword');
//let confirmPassword = document.signUpForm.confirmPassword.value;
let errorMessage = document.querySelectorAll('.Input-errorMessage');
const submitButton = document.querySelector('.SecondaryButton');

email.addEventListener('focus', function (e) {
  const parent = email.parentNode;
  const formWarning = parent.querySelector('.Form-warning');
  if(formWarning) {
    formWarning.classList.add('hidden');
    email.classList.remove('InputError');
  }
}, false);

email.addEventListener('blur', function (e) {
  let emailValue = document.signUpForm.email.value;
  if (emailValue.length == 0) {
    errorMessage[0].innerHTML = "Email is required.";
    email.classList.add('InputError');
  } else if (!email.validity.valid) {
    errorMessage[0].innerHTML = "A valid email address includes a @ sign.";
    errorMessage[0].className = "Input-errorMessage";   //What's the point of this?
    email.classList.add('InputError');
  }
}, false);

email.addEventListener('keyup', function (e) {
  let emailValue = document.signUpForm.email.value;
  if (email.validity.valid) {
    errorMessage[0].innerHTML = "";
    email.classList.remove('InputError');
    enableButton();
  } else if (!email.validity.valid && emailValue !=0 ) {
    errorMessage[0].innerHTML = "A valid email address includes a @sign.";
    email.classList.add('InputError');
  } else {
    errorMessage[0].innerHTML = "Email is required.";
    email.classList.add('InputError');
  }
}, false);

email.addEventListener('focus', function (e) {
  
})

password.addEventListener('blur', function (e) {
  let passwordValue = document.signUpForm.password.value;
  if (passwordValue.length == 0) {
    errorMessage[1].innerHTML = "Password is required.";
    password.classList.add('InputError');
  }
}, false);

password.addEventListener('keyup', function (e) {
  let passwordValue = document.signUpForm.password.value;
  let confirmPasswordValue = document.signUpForm.confirmPassword.value;
  if (passwordValue.length != 0 && confirmPasswordValue.length != 0 && passwordValue != confirmPasswordValue) {
    errorMessage[2].innerHTML = "Check to match the password";
    confirmPassword.classList.add('InputError');
  } else if(passwordValue.length != 0 && confirmPasswordValue.length != 0 && passwordValue == confirmPasswordValue) {
    errorMessage[2].innerHTML = "";
    confirmPassword.classList.remove('InputError');
    errorMessage[1].innerHTML = "";
    password.classList.remove('InputError');
    enableButton();
  } else if (passwordValue.length != 0) {
    errorMessage[1].innerHTML = "";
    password.classList.remove('InputError');
    enableButton();
  }
}, false);

//When password = confirm password, display a green check
//confirmPassword.addEventListener('keyup', function (e) {
  //let passwordValue = document.signUpForm.password.value;
  //let confirmPasswordValue = document.signUpForm.confirmPassword.value;
  //const parent = confirmPassword.parentNode;
  //const check = parent.querySelector('.fa-check');
  //if (passwordValue != 0 && passwordValue == confirmPasswordValue) {
    //errorMessage[2].innerHTML = "";
    //check.classList.remove('hidden');
  //}
//}, false);

confirmPassword.addEventListener('keyup', function(e) {
  let passwordValue = document.signUpForm.password.value;
  let confirmPasswordValue = document.signUpForm.confirmPassword.value;
  if (passwordValue == confirmPasswordValue && passwordValue != 0) {
    errorMessage[2].innerHTML = "";
    confirmPassword.classList.remove('InputError');
    enableButton();
  } else if (passwordValue != confirmPasswordValue) {
    errorMessage[2].innerHTML = "Check to match the password.";
    confirmPassword.classList.add('InputError');
  } else if (confirmPasswordValue.length == 0) {
    errorMessage[2].innerHTML = "Confirming password is required.";
    confirmPassword.classList.add('InputError');
  }
}, false);

confirmPassword.addEventListener('blur', function (e) {
  let passwordValue = document.signUpForm.password.value;
  let confirmPasswordValue = document.signUpForm.confirmPassword.value;
  if (confirmPasswordValue.length == 0) {
    errorMessage[2].innerHTML = "Confirming password is required.";
    confirmPassword.classList.add('InputError');
  } else if (passwordValue.length != 0 && passwordValue != confirmPasswordValue) {
    errorMessage[2].innerHTML = "Check to match the password.";
    confirmPassword.classList.add('InputError');
  } else if (passwordValue.length == 0) {
    errorMessage[1].innerHTML = "Password is required.";
    password.classList.add('InputError');
    enableButton();
  }
}, false);

//When password != confirm password on blur, display an error message
confirmPassword.addEventListener('blur', function (e) {
  
}, false);

function enableButton() {
  let passwordValue = document.signUpForm.password.value;
  let confirmPasswordValue = document.signUpForm.confirmPassword.value;
  console.log('I am here 1');
  if(email.validity.valid && passwordValue == confirmPasswordValue && passwordValue.length != 0) {
    submitButton.disabled = false;
    console.log('I am here 2');
  }
}

//enableButton();
