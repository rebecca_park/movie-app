from app import db

def update(movie_id, user_id, rating, category):
    query = ("UPDATE user_movie SET rating = %s, category = %s "
             "WHERE user_id = %s AND movie_id = %s")

    with db.cursor() as cursor:
        cursor.execute(query, (rating, category, user_id, movie_id))

    db.commit()

#Adds a movie into the category 'Watched'
def create(movie_id, user_id, rating):
    query = "INSERT INTO user_movie VALUES (0, %s, %s, %s, %s)"
    with db.cursor() as cursor:
        cursor.execute(query, (user_id, movie_id, rating, 'Watched'))

    db.commit()

#Adds a movie into the category 'Want to watch'
def add_want_to_watch(movie_id, user_id):
    query = ("INSERT INTO user_movie VALUES (0, %s, %s, 0, %s) "
             "ON DUPLICATE KEY UPDATE category = (%s), rating = 0")
    with db.cursor() as cursor:
        cursor.execute(query, (user_id, movie_id, 'Want to watch', 'Want to watch'))

    db.commit()

#Gets information of a movie existing in the list
def get(movie_id, user_id):
    with db.cursor() as cursor:
        query = ("SELECT user_id, movie_id, category, rating FROM user_movie "
                 "WHERE user_id=(%s) AND movie_id=(%s)")
        cursor.execute(query, (user_id, movie_id))
        result = cursor.fetchone()
    return result

def my_movies(user_id):
    with db.cursor() as cursor:
        query = ("SELECT movie.movie_id, movie.title, movie.poster, "
                 "user_movie.rating, user_movie.category FROM movie "
                 "INNER JOIN user_movie ON movie.movie_id = user_movie.movie_id "
                 "WHERE user_movie.user_id = %s")
        cursor.execute(query, (user_id,))

        movies = {
            "Watched": [],
            "Want_to_watch": []
        }

        for movie in cursor.fetchall():
            info = {
                "id": movie[0],
                "title": movie[1],
                "poster": movie[2],
                "rating": movie[3],
                "category": movie[4],
            }
            splits = movie[1].lower().split(' ')
            splits = '-'.join(splits)
            info['url'] = '/movie/' + splits
            if info["category"] == "Watched":
                movies["Watched"].append(info)
            else:
                movies["Want_to_watch"].append(info)
    return movies

def delete(movie_id, user_id):
    cursor = db.cursor()
    query = "DELETE FROM user_movie WHERE movie_id = %s AND user_id = %s"
    cursor.execute(query, (movie_id, user_id))
    db.commit()
    cursor.close()
    return
