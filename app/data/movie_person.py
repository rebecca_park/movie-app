from app import db

from . import person, user

def create(movie_id, person_id, role):
    query = "INSERT INTO movie_people VALUES (0, %s, %s, %s)"
    with db.cursor() as cursor:
        cursor.execute(query, (movie_id, person_id, role))
    db.commit()
    cursor.close()
    return

def get_by_slug(person_name):
    name = person_name.lower().split('-')
    name = ' '.join(name)

    persons = person.get_by_name(name)

    cursor = db.cursor()
    query = ("SELECT DISTINCT movie.movie_id, movie.title, movie.poster FROM movie INNER JOIN movie_people "
             "ON movie.movie_id = movie_people.movie_id "
             "WHERE movie_people.person_id = %s")
    cursor.execute(query, (persons['person_id'],))

    person_info = {
        "name": persons['name'],
        "picture": persons['picture'],
        "movies": []
    }

    for movie in cursor.fetchall():
        movie = {
            "title": movie[1],
            "poster": movie[2],
            "rating": 0
        }
        person_info['movies'].append(movie)
    cursor.close()
    return person_info

def user_person_overlap(person_name, email):
    name = person_name.lower().split('-')
    name = ' '.join(name)

    persons = person.get_by_name(name)
    user_id = user.get_id(email)

    cursor = db.cursor()
    query = ("SELECT DISTINCT movie.movie_id, movie.title, "
             "movie.poster, user_movie.rating FROM movie "
             "INNER JOIN movie_people "
             "ON movie.movie_id = movie_people.movie_id "
             "LEFT JOIN user_movie "
             "ON movie_people.movie_id = user_movie.movie_id "
             "WHERE movie_people.person_id = %s AND user_movie.user_id = %s")
    cursor.execute(query, (persons['person_id'], user_id))

    person_info = {
        "name": persons['name'],
        "picture": persons['picture'],
        "movies": []
    }

    for movie in cursor.fetchall():
        movie = {
            "title": movie[1],
            "poster": movie[2],
            "rating": movie[3],
        }
        splits = movie['title'].lower().split(' ')
        splits = '-'.join(splits)
        movie['url'] = '/movie/' + splits
        person_info['movies'].append(movie)
    cursor.close()
    return person_info
