from app import db

# Creates an account with empty profile picture
def create(email, password):
    with db.cursor() as cursor:
        cursor.execute("INSERT INTO user VALUES (%s, %s, '', 0)", (email, password))

    db.commit()

def sign_in(email, password):
    query = "SELECT picture FROM user WHERE username=%s AND password=%s"
    with db.cursor() as cursor:
        cursor.execute(query, (email, password))
        profile_picture = cursor.fetchone()
        cursor.close()
    email_exists = email_used(email)
    #1.successful login: profilePicture != null
    #2.account exists, but password is wrong: profilePicture = null && email_exists = True
    #3.account does not exist: profilePicture = null && email_exists = False
    print(profile_picture, 'This is profile picture!!!!!')
    return profile_picture[0], email_exists

# Gets the user id using the email
def get_id(email):
    with db.cursor() as cursor:
        cursor.execute("SELECT id FROM user WHERE username=%s", (email,))
        user_id = cursor.fetchone()

    return user_id[0]

#Checks if an account with the same email is already in use
def email_used(email):
    with db.cursor() as cursor:
        cursor.execute("SELECT username FROM user WHERE username=%s", (email,))
        already_exist = bool(cursor.fetchone())

    return already_exist
