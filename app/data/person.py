from app import db

def create(person, person_picture):
    with db.cursor() as cursor:
        cursor.execute("INSERT INTO people VALUES (0, %s, %s)", (person, person_picture))
        cursor.execute("SELECT LAST_INSERT_ID()")
        person_id = cursor.fetchone()[0]

    db.commit()
    return person_id

def get_by_name(name):
    cursor = db.cursor()
    cursor.execute("SELECT person_id, name, picture FROM people WHERE LOWER(name) = %s", (name,))
    fetched = cursor.fetchone()
    person = {
        'person_id': fetched[0],
        'name': fetched[1],
        'picture':  fetched[2]
    }
    return person

def search_person_movie(search, user_id):
    cursor = db.cursor()
    search = '%'+ search.lower() + '%'
    movies = []
    query =""
    if user_id != -1: 
        query = """
        SELECT
        DISTINCT movie.movie_id,
        movie.title,
        movie.poster,
        user_movie.rating
        FROM movie
        INNER JOIN movie_people ON movie.movie_id = movie_people.movie_id
        INNER JOIN people ON movie_people.person_id = people.person_id
        LEFT JOIN user_movie ON movie_people.movie_id = user_movie.movie_id
        WHERE LOWER(people.name) LIKE %s AND user_movie.user_id = %s
        """
        cursor.execute(query, (search, user_id))
        for movie_id, title, poster, rating in cursor.fetchall():
            movies.append({
                'id': movie_id,
                'title': title,
                'poster': poster,
                'rating': rating
            })
            splits = movies[-1]['title'].lower().split(' ')
            splits = '-'.join(splits)
            movies[-1]['url'] = '/movie/' + splits
        
    else :
        query = """
        SELECT 
        DISTINCT movie.movie_id,
        movie.title,
        movie.poster
        FROM movie
        INNER JOIN movie_people ON movie.movie_id = movie_people.movie_id
        INNER JOIN people ON movie_people.person_id = people.person_id
        WHERE LOWER (people.name) LIKE %s"""
        cursor.execute(query, (search,))
        for movie_id, title, poster in cursor.fetchall():
            movies.append({
                'id': movie_id,
                'title': title,
                'poster': poster,
                #'rating': rating
            })
            splits = movies[-1]['title'].lower().split(' ')
            splits = '-'.join(splits)
            movies[-1]['url'] = '/movie/' + splits
        

    #for movie_id, title, poster, rating in cursor.fetchall():
     #   movies.append({
      #      'id': movie_id,
       #     'title': title,
        #    'poster': poster,
         #   'rating': rating
       # })

        #splits = movies[-1]['title'].lower().split(' ')
        #splits = '-'.join(splits)
        #movies[-1]['url'] = '/movie/' + splits
        cursor.close()
    return movies
