from app import db

from . import user_movie

def create(title, year, movie_poster, cover_photo, story_line):
    with db.cursor() as cursor:
        cursor.execute("INSERT INTO movie VALUES (0, %s, %s, %s, %s, %s)",
                       (title, year, movie_poster, cover_photo, story_line))
        cursor.execute("SELECT LAST_INSERT_ID()")
        movie_id = cursor.fetchone()[0]

    db.commit()
    return movie_id

def search(keyword, user_id):
    query = "SELECT movie_id, title, poster FROM movie WHERE LOWER(title) LIKE %s"
    keyword_wrapped = '%' + keyword + '%'
    movies = []

    with db.cursor() as cursor:
        cursor.execute(query, (keyword_wrapped,))
 
        for movie_id, title, poster in cursor.fetchall():
            movies.append({
                'id': movie_id,
                'title': title,
                'poster': poster,
            })

            movie_on_watched_list = user_movie.get(movie_id, user_id)
            if movie_on_watched_list:
                movies[-1]['rating'] = movie_on_watched_list[3]
            else:
                movies[-1]['rating'] = 0

            splits = movies[-1]['title'].lower().split(' ')
            splits = '-'.join(splits)
            movies[-1]['url'] = '/movie/' + splits
    return movies

def get_by_slug(slug):
    name = slug.lower().split('-')
    name = ' '.join(name)

    with db.cursor() as cursor:
        #Get the movie information without the director and cast first
        query = ("SELECT movie_id, title, year, poster, coverPhoto, storyLine "
                 "FROM movie WHERE LOWER(title) = %s")
        cursor.execute(query, (name,))

        movie_id, title, year, poster, cover_photo, story_line = cursor.fetchone()

        #Now retrieve the director and cast information.
        query = ("SELECT people.name, people.picture, movie_people.role "
                 "FROM people INNER JOIN movie_people "
                 "ON people.person_id = movie_people.person_id "
                 "WHERE movie_people.movie_id = %s")
        cursor.execute(query, (movie_id,))

        movie = {
            "id": movie_id,
            "title": title,
            "year": year,
            "poster": poster,
            "coverPhoto": cover_photo,
            "storyLine": story_line,
            "actors": [],
            "directors": []
        }

        for person in cursor.fetchall():
            info = {
                "name": person[0],
                "picture": person[1],
                "role": person[2]
            }
            splits = person[0].lower().split(' ')
            splits = '-'.join(splits)
            info['url'] = '/person/' + splits
            if info['role'] == "Director":
                movie['directors'].append(info)
            else:
                movie['actors'].append(info)
    return movie
