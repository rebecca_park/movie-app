import pymysql
import pymysql.cursors
from flask import Flask, send_from_directory

server = Flask(__name__) # pylint: disable=invalid-name
db = pymysql.connect(host='localhost', # pylint: disable=invalid-name
                     user='root',
                     db='movieApp')

def serve_static(directory, path):
    return send_from_directory(directory, path,
                               add_etags=False,
                               cache_timeout=-1)

@server.route('/css/<path>')
def send_css(path):
    return serve_static('../css', path)

@server.route('/scripts/<path>')
def send_js(path):
    return serve_static('../scripts', path)

@server.route('/img/<path>')
def send_img(path):
    return serve_static('../img', path)
