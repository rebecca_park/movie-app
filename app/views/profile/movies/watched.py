from flask import request, redirect, url_for

from app import server
from app.views._utility import render_template
from app.views.movie import movie
from app.data import user_movie, user


@server.route('/profile/movies/watched', methods=['GET'])
def movies_watched():
    signedIn = request.cookies.get('signedIn')

    if not signedIn:
        return 'Not found', 404
        
    profilePic = request.cookies.get('profilePic')
    email = request.cookies.get('email')
    user_id = user.get_id(email)
    
    movies = user_movie.my_movies(user_id)
    movies_watched = movies["Watched"]

    return render_template('profile/movies/watched.html',
                           movies=movies_watched,
                           signedIn=True,
                           myMovie=True,
                           watched=True,
                           profilePic=profilePic)

@server.route('/profile/<movie_id>/want_to_watch', methods=['POST'])
def add_want_to_watch(movie_id):
    email = request.cookies.get('email')
    user_id = user.get_id(email)
    user_movie.add_want_to_watch(movie_id, user_id)
    return redirect(url_for('movies_watched'))
