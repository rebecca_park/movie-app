from flask import request, redirect, url_for

from app import server
from app.views._utility import render_template
from app.views.movie import movie
#from app.views.profile.movies import want_to_watch
from app.data import user_movie, user

#shows list of movies marked as 'Wantto watch' on 'My movies' page
@server.route('/profile/movies/want-to-watch', methods=['GET'])
def movies_want_to_watch(): 
    signedIn = request.cookies.get('signedIn')

    if not signedIn:
        return 'Not found', 404

    profilePic = request.cookies.get('profilePic')
    email = request.cookies.get('email')
    user_id = user.get_id(email)
    movies = user_movie.my_movies(user_id)
    movies_want_to_watch = movies['Want_to_watch']

    return render_template('profile/movies/want-to-watch.html',
                           movies=movies_want_to_watch,
                           signedIn=True,
                           myMovie=True,
                           want_to_watch=True,
                           profilePic=profilePic)

#adds a movie from 'Want to watch' to 'Watched' 
@server.route('/profile/<movie_id>/watched', methods=['POST'])
def add_watched(movie_id):
    movie.movie_movie_rate(movie_id)
    return redirect(url_for('movies_want_to_watch'))
