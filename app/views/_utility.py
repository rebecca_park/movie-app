import flask
from flask import request, url_for

def render_template(name, **kwargs):
    loggedIn = False
    signedIn = request.cookies.get('signedIn')
    if signedIn == 'True':
        kwargs["loggedIn"] = True
    else:
        kwargs["loggedIn"] = False

    return flask.render_template(name, **kwargs)
