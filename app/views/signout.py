from flask import redirect

from app import server

@server.route('/signOut')
def signOut():
    response = server.make_response(redirect('/search'))
    response.set_cookie('signedIn', value="False")
    response.set_cookie('profilePic', value='')
    return response
