from flask import request

from app import server
from app.views._utility import render_template
from app.data import movie, user, person

@server.route('/search')
def search():
    email = ''
    profilePic = ''
    myMovie = False
    user_id = -1
    
    signedIn = request.cookies.get('signedIn')
    if signedIn == 'True':
        myMovie = True,
        signedIn = True,
        email = request.cookies.get('email')
        profilePic = request.cookies.get('profilePic')
        user_id = user.get_id(email)
    else:
        signedIn = False
        
    search = request.args.get('search', '')
    movies = movie.search(search, user_id)
    person_movies = person.search_person_movie(search, user_id)
    allMovies = {}
    for _movie in movies:
        allMovies[_movie['title']] = _movie
    for _movie in person_movies:
        allMovies[_movie['title']] = _movie
    return render_template('search.html',
                       allMovies=allMovies.values(),
                       profilePic=profilePic,
                       search=search,
                       signedIn=signedIn)
