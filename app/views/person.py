from flask import request, jsonify

from app import server
from app.views._utility import render_template
from app.data import movie_person

@server.route('/person/<person_name>')
def person_profile(person_name):
    signedIn = request.cookies.get('signedIn')
    if signedIn == 'True':
        signedIn = True
        email = request.cookies.get('email')
        profilePic = request.cookies.get('profilePic')
        person = movie_person.user_person_overlap(person_name, email)
    else:
        signedIn = False
        person = movie_person.get_by_slug(person_name)
        profilePic=''
        
    return render_template('personPage.html',
                           person_name=person['name'],
                           movies=person['movies'],
                           person_profilePic=person['picture'],
                           signedIn=signedIn,
                           profilePic=profilePic)
