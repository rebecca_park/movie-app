from flask import request

from app import server, db
from app.views._utility import render_template
from app.data import user

import bcrypt

@server.route('/signUp', methods=['GET'])
def signUp():
    return render_template('signUp.html', signIn=True)

@server.route('/signUp', methods=['POST'])
def signUpSubmitted():
    email = request.form['email']
    password = request.form['password']
    confirmPassword = request.form['confirmPassword']
    email_error = ''
    password_error = ''
    hashed = bcrypt.hashpw(password.encode(), bcrypt.gensalt())

    email_used = user.email_used(email)
    # TODO: replace with data/ file and function
    if email_used:
        if (password != confirmPassword):
            password_error = "*Enter the same password as above." 
        email_error = "%s is already in use." % (email)
        return render_template("signUp.html", email_error=email_error, password_error=password_error, signIn=True)
    
    if password != confirmPassword:
        password_error = "*Enter the same password as above."
        return render_template("signUp.html", email_error=email_error, password_error=password_error, signIn=True)
    else:     
        user.create(email, hashed)
        return render_template('signUpSubmit.html',
                               email=email,
                               signIn=True)

    
