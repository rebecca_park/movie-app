from flask import request, redirect, url_for

from app import server
from app.data import movie, person, movie_person
from app.views._utility import render_template

@server.route('/movie/add', methods=['GET'])
def add_movie():
    signedIn = request.cookies.get('signedIn')
    if signedIn == 'True':
        signedIn = True
    else:
        signedIn = False

    profilePic = request.cookies.get('profilePic')
    return render_template('addMovie.html',
                           signedIn=signedIn,
                           addMovie=True,
                           profilePic=profilePic)

@server.route('/movie/add', methods=['POST'])
def add_movie_POST():
    title=request.form['title']
    year=request.form['year']

    directors=request.form.getlist('director')
    directorPictures=request.form.getlist('director-picture')
    actors=request.form.getlist('actor')
    actorPictures=request.form.getlist('actor-picture')
    roles=request.form.getlist('character')
    moviePoster=request.form['movie-poster']
    coverPhoto=request.form['cover-photo']

    storyLine='blah'

    movie_id = movie.create(title, year, moviePoster, coverPhoto, storyLine);

    for director, directorPicture in zip(directors, directorPictures):
        # TODO: does this person already exist? if so, don't recreate
        person_id = person.create(director, directorPicture)
        movie_person.create(movie_id, person_id, 'Director')

    for actor, actorPicture, role in zip(actors, actorPictures, roles):
        person_id = person.create(actor, actorPicture)
        movie_person.create(movie_id, person_id, role)

    #TODO: Split title by ' ' and join by '-'
    return redirect(url_for('movie_movie', slug=title))
