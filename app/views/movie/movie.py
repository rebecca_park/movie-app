from flask import request, jsonify, url_for, redirect

from app import server
from app.views._utility import render_template
from app.data import movie, user, user_movie, movie_person

@server.route('/movie/<slug>')
def movie_movie(slug):
    movies = movie.get_by_slug(slug)

    email = ''
    profilePic = ''
    userID = ''
    signedIn = request.cookies.get('signedIn')
    if signedIn == 'True':
        signedIn = True
        email = request.cookies.get('email')    
        profilePic = request.cookies.get('profilePic')
        userID = user.get_id(email)
    else:
        signedIn = False

    if not movies:
        return "Not found", 404
    else: 
        directors = movies['directors']
        actors = movies['actors']
        movied = {
            'movie_id': movies['id'],
            'title': movies['title'],
            'year': movies['year'],
            'poster': movies['poster'],
            'coverPhoto': movies['coverPhoto'],
            'plot': movies['storyLine']
        }
        watched = user_movie.get(movied['movie_id'], userID)
        if watched:
            movied['rating'] = watched[3]
        else:
            movied['rating'] = 0
    return render_template('moviePage.html',
                           directors=directors,
                           actors=actors,
                           movied=movied,
                           signedIn=signedIn,
                           profilePic=profilePic)

@server.route('/movie/<movie_id>/rate', methods=['POST']) #When a user rates a movie
def movie_movie_rate(movie_id):
    rating = request.get_json()["rating"]
    print(rating, 'this is rating');
    email = request.cookies.get('email')
    user_id = user.get_id(email)
    current_rating_exists = user_movie.get(movie_id, user_id)
    
    if current_rating_exists:
        user_movie.update(movie_id, user_id, rating, 'Watched')
    else:
        user_movie.create(movie_id, user_id, rating)

    return jsonify({'status': 'ok'})

@server.route('/movie/<movie_id>/unrate', methods=['POST'])
def movie_unrate(movie_id):
    email = request.cookies.get('email')
    user_id = user.get_id(email)
    user_movie.delete(movie_id, user_id)
    return jsonify({'status': 'ok'})
