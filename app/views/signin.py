import datetime

from flask import request, redirect, url_for

from app import server, db
from app.views._utility import render_template
from app.data import user

@server.route('/signIn', methods=['GET'])
def signIn():
    signedIn = request.cookies.get('signedIn')
    if signedIn == 'True':
        return redirect('/profile/movies/watched')
    else:
        return render_template('signIn.html',
                               signIn=True)

@server.route('/signIn', methods=['POST'])
def signInPost():
    email=request.form['email']
    password=request.form['password']
    profilePicture, email_exists = user.sign_in(email, password)
    print(profilePicture, 'This is profile picture!@!@!!')
    if profilePicture:
        response = server.make_response(redirect('/profile/movies/watched'))
        expire_date = datetime.datetime.now(datetime.timezone.utc)
        expire_date = expire_date + datetime.timedelta(hours=1)
        expire = expire_date
        print(expire, 'This is expireeeeee!!!!')
        response.set_cookie('signedIn', value='True', expires=expire)
        response.set_cookie('profilePic', value=profilePicture)
        response.set_cookie('email', value=email)
        return response
    elif (profilePicture == False and email_exists == True):
        return render_template('signIn.html', signIn=True, wrongPassword=True)
    elif email_exists == False:
        return render_template('signIn.html', signIn=True, noAccount=True)
        
