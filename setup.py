from setuptools import setup

setup(
    name='movie-app',
    version='0.1',
    install_requires=[
        'flask',
        'bcrypt',
        'pymysql',
    ],
)
