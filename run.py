from app import server, views

if __name__ == "__main__":
    server.run(debug=True, port=9000)
