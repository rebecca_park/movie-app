#!/usr/bin/env bash

set -e

ssh movie-app -- 'sudo apt-get update; \
                  sudo apt-get install -y python3 python3-pip python3-dev python3-setuptools libffi-dev uwsgi uwsgi-plugin-python3 nginx mysql-server; \
                  sudo mkdir -p /movie-app;
                  sudo chown -R server:server /movie-app'
