#!/usr/bin/env bash

set -e

rsync -rv --exclude '.git/*' --exclude '.env/*' . movie-app:/movie-app
ssh movie-app -- 'sudo cp /movie-app/_configs/movie-app.service /etc/systemd/system; \
                  (cd /movie-app && sudo pip3 install -e .); \
                  sudo systemctl daemon-reload; \
                  sudo systemctl restart movie-app; \
                  sudo cp /movie-app/_configs/nginx.conf /etc/nginx/sites-enabled/default; \
                  sudo service nginx restart'
